﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using HospitalManagementSystem.Data;
using Microsoft.AspNetCore.Mvc;
using HospitalManagementSystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace HospitalManagementSystem.Controller
{
    [Route("Api/login")]
    [ApiController]
    public class PatientLoginandSignupController : ControllerBase
    {
        
        private HospitalManagementSystemContext DB;
        public PatientLoginandSignupController(HospitalManagementSystemContext db)
        {
            DB = db;
        }

        [Route("InsertPatient")]
        [HttpPost]
        public object InsertPatient(PatientRegistration Reg)
        {
            try
            {
                PatientRegistration PL = new PatientRegistration();
                if (PL.PatientId == 0)
                {
                    PL.FirstName = Reg.FirstName;
                    PL.LastName = Reg.LastName;
                    PL.Email = Reg.Email;
                    PL.Mobile = Reg.Mobile;
                    PL.Gender = Reg.Gender;
                    PL.Age = Reg.Age;
                    PL.Password = Reg.Password;
                    DB.PatientRegistration.Add(PL);
                    DB.SaveChanges();
                    var name = PL.FirstName + " " + PL.LastName;
                    return new Response
                    { Status = "Success", Message = PL.Email, Name = name };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }
        //Login
        [Route("PatientLogin")]
        [HttpPost]
        public IActionResult PatientLogin(PatientLogin login)
        {

            var log = DB.PatientRegistration.Where(x => x.Email.Equals(login.Email) &&
                      x.Password.Equals(login.Password)).FirstOrDefault();
            if(log == null)
            {
                return NotFound(new {
                    Status = "NotFound",
                    Message = "Enter valid user id and password."
                });  
            }

            var getName = log.FirstName + " " + log.LastName;

            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("efthfteyfw72728782877632")), SecurityAlgorithms.HmacSha256Signature);
            var token = new JwtSecurityToken(
                   issuer: "HMS",
                   audience: "User",
                   claims: null,
                   notBefore: DateTime.Now, 
                   expires: DateTime.Now.AddMonths(2),
                   signingCredentials: signingCredentials
                );
            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            return Ok(new
            {
                Status = "Success",
                Message = login.Email,
                Name = getName,
                JwtToken = jwtToken
            });
         
        }
    }
}
