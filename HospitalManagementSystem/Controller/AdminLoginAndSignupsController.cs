﻿using System;
using System.Collections.Generic;
using System.Linq;
using HospitalManagementSystem.Data;
using HospitalManagementSystem.Models;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;

namespace HospitalManagementSystem.Controller
{
    [Route("Api/Admin")]
    [ApiController]
    public class AdminLoginandSignupController : ControllerBase
    {
        public AdminLoginandSignupController(HospitalManagementSystemContext DB)
        {
            this.DB = DB;
        }
        private HospitalManagementSystemContext DB;

        [Route("AdminSignup")]
        [HttpPost]
        public object InsertAdmin(AdminLoginAndSignup Reg)
        {
            try
            {
                AdminLoginAndSignup PL = new AdminLoginAndSignup();
                if (PL.Id == 0)
                {

                    PL.Email = Reg.Email;
                    PL.Password = Reg.Password;
                    DB.AdminLoginAndSignup.Add(PL);
                    DB.SaveChanges();
                    return new Response
                    { Status = "Success", Message = "Admin Signup SuccessFully Saved." };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }
        //Login
        [Route("AdminLogin")]
        [HttpPost]
        public IActionResult AdminLogin(PatientLogin login)
        {
            var log = DB.AdminLoginAndSignup.Where(x => x.Email.Equals(login.Email) &&
                      x.Password.Equals(login.Password)).FirstOrDefault();

            var claims = new List<Claim>
            {
                new Claim("Admin", "true")
            };

            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("efthfteyfw72728782877632")), SecurityAlgorithms.HmacSha256Signature);
            var token = new JwtSecurityToken(
                   issuer: "HMS",
                   audience: "User",
                   claims: claims,
                   notBefore: DateTime.Now,
                   expires: DateTime.Now.AddMonths(2),
                   signingCredentials: signingCredentials
                );
            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            if (log != null)
            {
                return Ok(new
                {
                    Status = "Success",
                    Message = "Admin Login Successfully.",
                    JwtToken = jwtToken
                });
                
            }
            return BadRequest(new
            {
                Status = "BadRequest",
                Message = "Admin Login failed.",
            });
        }
    }
}
